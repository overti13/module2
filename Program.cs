using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;

namespace Module2
{
    public class Program
    {
        static void Main(string[] args)
        {
            //реалзация кода
        }

        public int GetTotalTax(int companiesNumber, int tax, int companyRevenue)
        {
            return (int)(Math.Round(companiesNumber * companyRevenue * tax * 0.01));
        }

        public string GetCongratulation(int input)
        {
            if (input % 2 == 0 && input >= 18) return "Поздравляю с совершеннолетием!";
            else if (input % 2 != 0 && input > 12 && input < 18) return "Поздравляю с переходом в старшую школу!";
            else return "Поздравляю с " + input + "-летием!";
        }

        public double GetMultipliedNumbers(string first, string second)
        {
            double s, f, second2;
            string[] cifr = first.Split(",.".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            int first2, first1;
            Int32.TryParse(cifr[0], out first1);
            if (first1 == 0)
                throw new ArgumentException();
            if (cifr.Length != 1)
            {
                Int32.TryParse(cifr[1], out first2);
                f = first1 + first2 * Math.Pow(10, -cifr[1].Length);
            }
            else f = first1;
            cifr = second.Split(",.".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            Int32.TryParse(cifr[0], out int second1);
            if (second1 == 0)
                throw new ArgumentException();
            if (cifr.Length != 1)
            {
                double.TryParse(cifr[1], out second2);
                s = second1 + second2 * Math.Pow(10, -cifr[1].Length);
            }
            else s = second1;
            return f * s;
        }

        public double GetFigureValues(FigureEnum figureType, ParameterEnum parameterToCompute, Dimensions dimensions)
        {
            if (parameterToCompute == ParameterEnum.Square)
            {
                if (figureType == FigureEnum.Triangle)
                {
                    if (dimensions.Height == 0)
                    {
                        double p = 1.0 / 2 * (dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide);
                        return Math.Round(Math.Sqrt(p * (p - dimensions.FirstSide) * (p - dimensions.SecondSide) * (p - dimensions.ThirdSide)));
                    }
                    else return Math.Round(1.0 / 2 * dimensions.FirstSide * dimensions.Height);
                }
                else if (figureType == FigureEnum.Rectangle) return Math.Round(dimensions.FirstSide * dimensions.SecondSide);
                else
                {
                    if (dimensions.Radius == 0)
                    {
                        return Math.Round(Math.PI * Math.Pow(dimensions.Diameter, 2) / 4);
                    }
                    else return Math.Round(Math.PI * Math.Pow(dimensions.Radius, 2));
                }
            }
            else 
            {
                if (figureType == FigureEnum.Triangle) return Math.Round(dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide);
                else if (figureType == FigureEnum.Rectangle) return Math.Round(2.0 * (dimensions.FirstSide + dimensions.SecondSide));
                else
                {
                    if (dimensions.Radius == 0) return Math.Round(Math.PI * dimensions.Diameter);
                    else return Math.Round(2.0 * dimensions.Radius * Math.PI);
                }
            }
        }
    }
}
